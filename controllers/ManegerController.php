<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\maneger\AddForm;
use app\models\maneger\Team;
use app\models\maneger\Players;
use app\models\maneger\AddPlayersForm;

class ManegerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionShowTeam(){
        
       if ($id = Yii::$app->request->get('id')){
           
          Players::deleteAll(['team_id' => $id]);
           
           $team = Team::find()->where(['id' => $id])->one();
           $team->delete();
    
       }
       
      
       $team = Team::find()->asArray()->all();
        
        return $this->render('show_team', compact('team'));
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionAddTeam(){
        
        $urlButton = 'index.php?r=maneger/add-team';
        $button = 'Додати';
        $model = new AddForm();
        $edit_data_team = 'Додати команду';
       
     
        if($model->load(Yii::$app->request->post()) && $model->save()){
            
            return $this->redirect('index.php?r=maneger/show-team');
           
        }
        
        return $this->render('add_team', compact('model','urlButton','button','edit_data_team'));
       
    }

   
    public function actionShowPlayers(){
        
        if($id = Yii::$app->request->get('id')){
            
         // Players::deleteAll(['team_id' => $id]);
           
          $players = Players::find()->where(['id' => $id])->one();
          $players->delete();   
        }
        
        $team_id = Yii::$app->request->get('team_id');
        $players = Players::findAll(['team_id'=>$team_id]);
        return $this->render('show_players', compact('players','team_id'));
    }
    
    
    public function actionEditTeam(){
        
        $model = new AddForm();
        $button = 'Змінити';
        $urlButton = 'index.php?r=maneger/edit-team&id='.Yii::$app->request->get('id');
        $edit_data_team = 'Змінити дані команди';
        if(Yii::$app->request->post()){
            
            $teamAR = AddForm::find()->where(['id' =>Yii::$app->request->get('id')])->one();
            $teamAR->load(Yii::$app->request->post());
            $teamAR->save();
            return $this->redirect('index.php?r=maneger/show-players');
           
        }
        
     
        return $this->render('add_team',compact('button','model','urlButton','edit_data_team'));
    }

    
    public function actionAddPlayers(){
        $model = new AddPlayersForm();
        $button = 'Додати';
        $team_id = Yii::$app->request->get('team_id');
        $urlButton = '/index.php?r=maneger/add-players&team_id='.$team_id;
        $edit_players = 'Додати гравця';
        if($model->load(Yii::$app->request->post())){
            $model->team_id = $team_id;
            $model->save();
            return $this->redirect('index.php?r=maneger/show-players&team_id='.$team_id);
           
        }
        
        return $this->render('add_players', compact('model','urlButton','button','edit_players'));
        
    }
    
    public function actionEditPlayers(){
        
        $model = new AddPlayersForm();
        $button = 'Змінити';
        $urlButton = 'index.php?r=maneger/edit-players&team_id='.Yii::$app->request->get('team_id');
        $urlButton .= '&edit_id='.Yii::$app->request->get('edit_id'); 
        $edit_players = 'Змінити дані гравця';
        if(Yii::$app->request->post()){
            
            $playersAR = AddPlayersForm::find()->where(['id' =>Yii::$app->request->get('edit_id')])->one();
            $playersAR->load(Yii::$app->request->post());
            $playersAR->team_id = Yii::$app->request->get('team_id');
            $playersAR->save();
        
            return $this->redirect('index.php?r=maneger/show-players&team_id='.Yii::$app->request->get('team_id'));
           
        }
        
     
        return $this->render('add_players',compact('button','model','urlButton','edit_players'));
        
        
    }
}

