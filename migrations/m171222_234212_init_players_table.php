<?php

use yii\db\Migration;

/**
 * Class m171222_234212_init_players_table
 */
class m171222_234212_init_players_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_234212_init_players_table cannot be reverted.\n";

        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up(){
        
        $this->createTable(
                'players',
                [
                    'id' => 'pk',
                    'team_id' => 'int',
                    'firstname' => 'string',
                    'secondname' => 'string',
                    'birthday' => 'date',
                    'position' => 'string',
                ],
                'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
                );
        
         
        
        $this->addForeignKey('players_of_team','players','team_id','team','id');

    }

    public function down()
    {
        $this->dropForeignKey('players_of_team','players');
        $this->dropTable('players');
    }
    
}
