<?php

use yii\db\Migration;

/**
 * Class m171222_233114_init_team_table
 */
class m171222_233114_init_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_233114_init_team_table cannot be reverted.\n";

        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up(){
        
        $this->createTable(
                'team',
                [
                    'id'=>'pk',
                    'name'=>'string',
                    'foundation'=>'date',
                ],
                'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
                );

    }

    public function down()
    {
        $htis->dropTable('team');
    }
    
}
