<?php

/* @var $this yii\web\View */

use yii\helpers\Html;


?>
<div class="table">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo Html::a('Додати гравця', ['maneger/add-players', 'team_id' => $team_id], ['class' => 'button']) ?>

    <table border="1">
        <tr>
            <th>id</th>
            <th>id команди</th>
            <th>Ім'я гравця</th>
            <th>Прізвище гравця</th>
            <th>День народження</th>
            <th>Позиція на полі</th>
            <th>Керування</th>
                
        </tr>
        <?php 
        
        foreach ($players as $value){
           echo "<tr  style = 'cursor: pointer;'>";
           foreach ($value as $val){
               echo "<th>";
               echo "$val";
               echo "</th>";
           }
           
           
           echo "<th>";
           echo Html::a('Редагувати ', ['maneger/edit-players','edit_id' =>$value['id'],'team_id'=>$team_id, ['class' => 'profile-link']]);
           echo Html::a('Видалити', ['maneger/show-players','team_id'=>$team_id,'id' => $value['id']], ['class' => 'profile-link']);
           echo "</th>";
           
            
            echo "</tr>";
        }
        
        ?>
    </table>
    
</div>


