<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $edit_players;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php $form = ActiveForm::begin([
        'action' => $urlButton,
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'firstname')->textInput(['autofocus' => true])->label('І\'мя') ?>
        <?= $form->field($model, 'secondname')->textInput()->label('Прізвище') ?>
        <?= $form->field($model, 'birthday')->textInput(['placeholder' => 'YYYY-MM-DD \'Наприклад 1990-10-10\''])->label('День народження') ?>
        <?= $form->field($model, 'position')->textInput()->label('Позиція на полі') ?>
        <?= $form->field($model, 'team_id')->hiddenInput(['value'=> Yii::$app->request->get('team_id')])->label(false) ?>
       
    
       

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton($button, ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

   
</div>


