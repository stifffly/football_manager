<?php

/* @var $this yii\web\View */

use yii\helpers\Html;


?>
<div class="table">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php echo Html::a('Додати команду', ['maneger/add-team'], ['class' => 'button']) ?>

    <table border="1">
        <tr>
            <th>id</th>
            <th>Ім'я команди</th>
            <th>Дата заснування</th>
            <th>Керування</th>
                
        </tr>
        <?php 
        
        foreach ($team as $value){
           echo "<tr  style = 'cursor: pointer;' onclick=\"document.location = './index.php?r=maneger/show-players&team_id=$value[id]';\">";
           foreach ($value as $val){
               echo "<th>";
               echo "$val";
               echo "</th>";
           }
           echo "<th>";
           echo Html::a('Редагувати ', ['maneger/edit-team','id' => $value['id']], ['class' => 'profile-link']);
           echo Html::a('Видалити', ['maneger/show-team','id' => $value['id']], ['class' => 'profile-link']);
           echo "</th>";
            
            echo "</tr>";
        }
        
        ?>
    </table>
    
</div>
