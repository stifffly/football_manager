<?php

namespace app\models\maneger;

use Yii;
use yii\db\ActiveRecord;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AddPlayersForm extends ActiveRecord
{
  
       public static function tableName(){
        
        return 'players';
    }

   


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['team_id','number'],
            ['firstname','string'],
            ['secondname','string'],
            ['birthday','date','format'=>'yyyy-mm-dd'],
            ['position','string'],
           
        ];
    }

}


